//
//  DeckView.swift
//  SetGame
//
//  Created by Ahmed Sharf on 1/1/22.
//

import UIKit

class DeckView: UIView {
    
    var cards = [CardButton]() { didSet { setNeedsLayout() } }
    var dealingCards = [CardButton]()
    
    lazy private var grid = Grid(layout: Grid.Layout.aspectRatio(LayoutConstants.cardAspectRatio), frame: bounds)
    
    override func layoutSubviews() {
        super.layoutSubviews()
        arrangeCards()
    }

    private func arrangeCards() {
        var cardsToDeal = [CardButton]()
        
        let boundsChanged = grid.frame != bounds
        grid.frame = bounds
        grid.cellCount = cards.count
        
        for view in subviews {
            if let cardView = view as? CardButton {
                if !cards.contains(cardView) {
                    view.removeFromSuperview()
                }
            }
        }
        
        for (index, card) in self.cards.enumerated() {
            if !boundsChanged {
                guard !self.dealingCards.contains(card) else {
                    continue
                }
            }
            if !self.subviews.contains(card)  {
                cardsToDeal.append(card)
                self.dealingCards.append(card)
                continue
            }
            if let frame = self.grid[index] {
                card.frame = frame.insetBy(dx: LayoutConstants.spacing, dy: LayoutConstants.spacing) }
        }
        
        self.dealCards(cardsToDeal)
    }
    
    private func dealCards(_ cardsToDeal: [CardButton]) {
        for card in cardsToDeal {
            if let cardIndex = self.cards.firstIndex(of: card), let frame = self.grid[cardIndex] {
                self.addSubview(card)
                self.sendSubviewToBack(card)
                card.transform = .identity
                card.frame = frame.insetBy(dx: LayoutConstants.spacing, dy: LayoutConstants.spacing)
                
            }
            UIView.transition(with: card,
                              duration: 0.3,
                              options: .transitionFlipFromLeft,
                              animations: nil,
                              completion: { position in
                                if let dealingIndex = self.dealingCards.firstIndex(of: card) {
                                    self.dealingCards.remove(at: dealingIndex)
                                }
                              })
        }
    }
    
    struct LayoutConstants {
        static let spacing: CGFloat = 5.0
        static let cardAspectRatio: CGFloat = 0.7
    }
}
