//
//  CardButton.swift
//  SetGame
//
//  Created by Ahmed Sharf on 12/27/21.
//

import UIKit

class CardButton: UIButton {
    var card: Card? { didSet{ setNeedsDisplay() } }

    init(card: Card) {
        let newCard = Card(shape: card.shape, numberOfShapes: card.numberOfShapes, shade: card.shade, color: card.color)
        self.card = newCard
        
        super.init(frame: CGRect())
        contentMode = .redraw
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func drawCard() {
        guard let card = card else { return }

        for origin in shapeOrigins {
            let shapeRect = CGRect(origin: origin, size: CGSize(width: centerX, height: centerX))
            
            var path = UIBezierPath()
            cardColor.set()
            
            switch card.shape {
            case .circle: path = UIBezierPath(ovalIn: shapeRect)
            case .diamond: path = UIBezierPath(diamondIn: shapeRect)
            case .rectangle: path = UIBezierPath(rect: shapeRect)
            }
            
            UIGraphicsGetCurrentContext()?.saveGState()
            
            switch card.shade {
            case .open: break
            case .solid: path.fill()
            case .striped: path.stripe(withGapOf: Int(bounds.height)/15);
            }
            
            path.stroke()
            UIGraphicsGetCurrentContext()?.restoreGState()
        }
        let border = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        border.lineWidth = 2
        border.addClip()
        cardColor.set()
        border.stroke()
    }
    
    override func draw(_ rect: CGRect) {
        let roundedRect = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        roundedRect.addClip()
        UIColor.white.setFill()
        roundedRect.fill()
        
        drawCard()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        setNeedsDisplay()
    }
}

extension CardButton {
    private struct SizeRatio {
        static let cornerRadiusToBoundsWidth: CGFloat = 0.06
    }
    
    private var cornerRadius: CGFloat {
        return bounds.size.width * SizeRatio.cornerRadiusToBoundsWidth
    }
    
    private var space: Int {
        return 8
    }
    
    private var centerY: CGFloat {
        return bounds.height/2
    }
    private var centerX: CGFloat {
        var x = CGFloat(0)
        guard let card = card else { return x }
        switch card.numberOfShapes {
        case .one:
            x = bounds.width/2
        case .two:
            x = bounds.width/3
        case .three:
            x = bounds.width/4
        }
        return x
    }
    private var halfSide: CGFloat {
        return centerX/2
    }
    
    private var startPoint: CGPoint {
        var point = CGPoint()
        guard let card = card else { return point }
        switch card.numberOfShapes {
        case .one:
            point = CGPoint(x: centerX - halfSide, y: centerY - halfSide)
        case .two:
            point = CGPoint(x: centerX - halfSide - CGFloat(space), y: centerY - halfSide)
        case .three:
            point = CGPoint(x: centerX - halfSide - CGFloat(space), y: centerY - halfSide)
        }
        return point
    }
    
    private var secondPoint: CGPoint {
        var point = CGPoint()
        guard let card = card else { return point }
        if card.numberOfShapes.rawValue == 2 {
            point = CGPoint(x: centerX + halfSide + CGFloat(space) , y: centerY - halfSide)
        } else { //card.numberOfShapes == 3
            point = CGPoint(x: centerX + halfSide , y: centerY - halfSide)
        }
        return point
    }
    
    private var thirdPoint: CGPoint {
        var point = CGPoint()
        point = CGPoint(x: centerX + (halfSide*3) + CGFloat(space), y: centerY - halfSide)
        return point
    }
    
    private var shapeOrigins: [CGPoint] {
        var origins = [startPoint]
        guard let card = card else { return origins }
        if card.numberOfShapes.rawValue > 1 {
            origins.append(secondPoint)
        }
        
        if card.numberOfShapes.rawValue > 2 {
            origins.append(thirdPoint)
        }
        return origins
    }
    
    private var cardColor: UIColor {
        switch card?.color {
        case .green: return .systemGreen
        case .red: return .systemRed
        case .purple: return .systemPurple
        case .none: return .clear
        }
    }
}

extension CardButton {
    func setButton(with card: Card) {
        self.isEnabled = true
        self.card = card
    }
}
