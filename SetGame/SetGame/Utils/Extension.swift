//
//  Extension.swift
//  SetGame
//
//  Created by Ahmed Sharf on 12/29/21.
//

import UIKit

extension UIButton {
    func disableButton() {
        self.alpha = 0.5
        self.isEnabled = false
    }
    
    func enableButton() {
        self.alpha = 1
        self.isEnabled = true
    }
    
    func removeBorder() {
        self.layer.borderWidth = 0
    }
    
    func setBorder(with value: CGFloat) {
        self.layer.borderWidth = value
    }
    
    func setColor(with color: CGColor) {
        self.layer.borderColor = color
    }
    
    func setBorderAndColorWidth(with borderValue: CGFloat, color: CGColor) {
        self.setBorder(with: borderValue)
        self.setColor(with: color)
    }
}

extension UIBezierPath {  
    convenience init(diamondIn rect: CGRect) {
        self.init()
        move(to: CGPoint(x: rect.minX, y: rect.midY))
        addLine(to: CGPoint(x: rect.midX, y: rect.minY))
        addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
        addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
        close()
    }
    
    func stripe(withGapOf distance: Int) {
        addClip()
        
        for stripeYPosition in stride(from: bounds.minY, through: bounds.maxY, by: CGFloat(distance)) {
            move(to: CGPoint(x: bounds.minX, y: stripeYPosition))
            addLine(to: CGPoint(x: bounds.maxX, y: stripeYPosition))
        }
    }
}
