//
//  ViewController.swift
//  SetGame
//
//  Created by Ahmed Sharf on 12/27/21.
//

import UIKit

class ViewController: UIViewController {

    var deck = CardDeck()
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var remainingCardsLabel: UILabel!
    @IBOutlet weak var deckView: DeckView! {
        didSet {
            let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(deal3MorePressed))
            swipeDown.direction = .down
            deckView.addGestureRecognizer(swipeDown)
            
            let rotation = UIRotationGestureRecognizer(target: self, action: #selector(shuffleCards(recognizer:)))
            deckView.addGestureRecognizer(rotation)
        }
    }
    
    private var cardButtons: [CardButton] = []

    @IBOutlet weak var deal3MoreButton: UIButton!
    
    @IBOutlet weak var endGameView: UIView!
    @IBOutlet weak var endGameImageView: UIImageView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        deal3MoreButton.addTarget(self, action: #selector(deal3MorePressed), for: .touchUpInside)
        updateViewFromModel()
    }

    @IBAction func newGamePressed(_ sender: UIButton) {
        endGameViewsVisibility(with: true)
        deck.newGame()
        checkToDisableDeal3MoreButton()
        hideButtonsBorder(with: 0..<cardButtons.count)
        cardButtons.removeAll()
        updateViewFromModel()
    }

    @objc func deal3MorePressed() {
        deal3More()
    }
    private func deal3More() {
        if deal3MoreButton.isEnabled {
            deck.drawThreeCards()
            showNewCards()
            checkToDisableDeal3MoreButton()
            updateLabels()
        }
    }
    
    @objc func shuffleCards(recognizer: UIRotationGestureRecognizer) {
            switch recognizer.state {
            case .ended:
                deck.shuffleCards()
                cardButtons.removeAll()
                updateViewFromModel()
            default: break
        }
    }
    
    @objc func touchCard(recognizer: UIGestureRecognizer) {
        if deck.selectedCardsIndecies.count == 0 {
            hideButtonsBorder(with: 0..<cardButtons.count)
        }
        
        guard let cardTapped = recognizer.view as? CardButton else { return }
        guard let cardNumber = cardButtons.firstIndex(of: cardTapped) else { return }
        for index in deck.selectedCardsIndecies { //handle if user select selected card
            if cardNumber == index {
                cardTapped.removeBorder()
                deck.unselectCard(at: cardNumber)
                updateLabels()
                return
            }
        }
        deck.selectCard(at: cardNumber) { (res) in
            if let res = res {
                if res {
                    for index in deck.selectedCardsIndecies {
                        cardButtons[index].removeBorder()
                    }
                    checkToDisableDeal3MoreButton()
                    updateLabels()
                    clearButtons(with: deck.selectedCardsIndecies)

                    if deck.checkIfEndGame() {
                        endGameViewsVisibility(with: false)
                    }
                } else {
                    for index in deck.selectedCardsIndecies {
                        cardButtons[index].setBorderAndColorWidth(with: 3, color: UIColor.red.cgColor)
                    }
                    updateLabels()
                }
            } else {
                cardTapped.setBorderAndColorWidth(with: 3, color: UIColor.blue.cgColor)
            }
        }
    }
    
    private func clearButtons(with range: [Int]) {
        var tempArr = range
        tempArr.sort(by: >)
        for index in tempArr {
            cardButtons.remove(at: index)
            deckView.cards.remove(at: index)
        }
    }
    
    private func hideButtonsBorder(with range: Range<Int>) {
        for index in range {
            cardButtons[index].removeBorder()
        }
    }
    
    private func checkToDisableDeal3MoreButton() {
        if deck.playedCards.count > 22 || deck.cardsCount < 2{
            deal3MoreButton.disableButton()
        } else {
            deal3MoreButton.enableButton()
        }
    }
    
    private func showNewCards() {
        for index in deck.playedCards.suffix(3).indices {
            let b = CardButton(card: deck.playedCards[index])
            b.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchCard(recognizer:))))
            cardButtons.append(b)
            deckView.cards.append(b)
        }
    }
    
    private func updateViewFromModel() {
        updateLabels()
        if cardButtons.isEmpty {
            for index in deck.playedCards.indices {
                let b = CardButton(card: deck.playedCards[index])
                b.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touchCard(recognizer:))))
                cardButtons.append(b)
            }
            deckView.cards = cardButtons
        }
    }
    
    private func updateLabels() {
        scoreLabel.text = "Score: \(deck.score)"
        remainingCardsLabel.text = "Cards in deck: \(deck.cardsCount)"
    }
    
    private func endGameViewsVisibility(with value: Bool) {
        endGameView.isHidden = value
        endGameImageView.isHidden = value
    }
    
}
