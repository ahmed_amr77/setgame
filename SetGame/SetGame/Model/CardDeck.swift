//
//  CardDeck.swift
//  SetGame
//
//  Created by Ahmed Sharf on 12/27/21.
//

//The only actual functionality your Model has is selecting cards to try to match && dealing three new cards on demand.

import Foundation

class CardDeck {
    private var cards = [Card]()
    private(set) var playedCards = [Card]()
    
    private(set) var selectedCardsIndecies = [Int]()
    private(set) var score = 0
    
    var cardsCount: Int {
        return cards.count
    }
    
    init() {
        initializeCards()
    }
    
    private func initializeCards() {
        for shape in Card.Shape.allCases {
            for numberOfShapes in Card.NumberOfShapes.allCases {
                for shade in Card.Shade.allCases {
                    for color in Card.Color.allCases {
                        cards.append(Card(shape: shape, numberOfShapes: numberOfShapes, shade: shade, color: color))
                    }
                }
            }
        }
        cards.shuffle()
        for _ in 1...12 {
            playedCards.append(cards.remove(at: Int.random(in: 0..<cards.count)))
        }
    }
    
    func shuffleCards() {
        playedCards.shuffle()
    }
    
    func newGame() {
        cards.removeAll()
        playedCards.removeAll()
        selectedCardsIndecies.removeAll()
        score = 0
        initializeCards()
    }
    
    func selectCard(at index: Int, completion: (Bool?) -> Void) { //handle from controller if user select selected card
        selectedCardsIndecies.append(index)
        if selectedCardsIndecies.count < 3 {
            completion(nil)
        } else {  //if let res = res { if res {deck.draw3More; deck.checkIfEnd; redraw [to remove old cards & draw the news]} else {show that mismatch *red border*} }
            completion(checkForMatch())
            selectedCardsIndecies.removeAll()
        }
    }
    
    func unselectCard(at index: Int) {
        score -= 1
        if let index = selectedCardsIndecies.firstIndex(of: index) {
            selectedCardsIndecies.remove(at: index)
        }
    }
    
    func drawThreeCards() {
        guard cards.count > 2 else { return }
        if checkAllCases(with: Array(playedCards.indices)) {
            score -= 3
        }
        
        var drawenCards: [Card] = []
        for _ in 1...3 {
            drawenCards.append(cards.remove(at: Int.random(in: 0..<cards.count)))
        }
        playedCards.append(contentsOf: drawenCards)
    }
    
    private func checkForMatch() -> Bool{
        if checkAllCases(with: selectedCardsIndecies) {
            score += 3
            var tempArr = selectedCardsIndecies
            tempArr.sort(by: >)
            for index in tempArr { //from controller remove cards or hide (redraw??)
                playedCards.remove(at: index)
            }
            return true
        } else {        //if no match, FROM controller deselect cards
            score -= 5
            return false
        }
    }
    private func checkAllCases(with indecies: [Int]) -> Bool {
        var colorSet = Set<Card.Color>()
        var shapeSet = Set<Card.Shape>()
        var shadeSet = Set<Card.Shade>()
        var numberSet = Set<Card.NumberOfShapes>()
        
        for index in indecies {
            colorSet.insert(playedCards[index].color)
            shapeSet.insert(playedCards[index].shape)
            shadeSet.insert(playedCards[index].shade)
            numberSet.insert(playedCards[index].numberOfShapes)
        }
        
        if colorSet.count == 1 || colorSet.count == 3 {
            if shapeSet.count == 1 || shapeSet.count == 3 {
                if shadeSet.count == 1 || shadeSet.count == 3 {
                    if numberSet.count == 1 || numberSet.count == 3 {
                        return true
                    }
                }
            }
        }
        return false
    }
    
    func checkIfEndGame() -> Bool {
        if cards.count == 0 {
            if playedCards.count > 2 {
                for index1 in 0..<playedCards.count {
                    for index2 in index1+1..<playedCards.count {
                        for index3 in index2+1..<playedCards.count {
                            if checkAllCases(with: [index1, index2, index3]) {
                                return false // not end cuz there is still available match
                            }
                        }
                    }
                }
            }
            return true
        }
        return false
    }
}

