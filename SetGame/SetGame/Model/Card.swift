//
//  Card.swift
//  SetGame
//
//  Created by Ahmed Sharf on 12/27/21.
//

import Foundation

struct Card {
    let shape: Shape
    let numberOfShapes: NumberOfShapes
    let shade: Shade
    let color: Color
    
    enum Shape: CaseIterable {
        case rectangle
        case circle
        case diamond
    }
    
    enum NumberOfShapes: Int, CaseIterable {
        case one = 1
        case two
        case three
    }
    
    enum Shade: CaseIterable {
        case solid
        case striped
        case open
    }
    
    enum Color: CaseIterable {
        case red
        case green
        case purple
    }
}
